#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import zipfile
import zlib
from mimetypes import MimeTypes
import xml.etree.ElementTree as ET
from dudeit import dude
import time
import datetime

class EpubStudio(zipfile.ZipFile):
    """Made to manipulate and make batch changes in any epub file content
    foe regexp replacements:
    - need a regexp_epub.ini for changes in XHTML files ;
    - need a regexp_opf.ini for changes in opf file ;
    - need a regexp_toc_ncx.ini for changes in ncx files.
    """

    def __init__(self, inputfile):
        self.name = inputfile
        self.dir = os.path.split(self.name)[0]
        zipfile.ZipFile.__init__(self, inputfile, "a")
        # Check if there are the needed regexp files in the same folder
        for filename in os.listdir(self.dir):
            # print(filename)
            try:
                if filename == "regexp_epub.ini":
                    self.regexp_epub = True
                    # print("Base OK")
                if filename == "regexp_opf.ini":
                    self.regexp_opf = True
                    # print("OPF OK")
                if filename == "regexp_toc_ncx.ini":
                    self.regexp_toc_ncx = True
                    # print("NCX OK")
            except AttributeError:
                pass

    def clone(self):
        """Create a temporary filename in the very same folder as the initial
        with the same name with "_temp" before its extension
        Return a string made of the tempfile name and its abspath
        """
        workingfile = os.path.split(self.name)
        tempfile = os.path.join(str(workingfile[0]), "".join(workingfile[1].split(".")[:-1]) + "_temp." + "".join(workingfile[1].split(".")[-1:]))
        return tempfile

    def cleanup(self, regexp=""):
        """
        Clean the content of xhtml files of an epub file with a regexp ini file
        """
        # On vérifie si on a déjà le fichier de regexp, ou s'il est en argument
        try:
            if self.regexp_epub:
                regexp = os.path.join(self.dir, "regexp_epub.ini")
                # print("Fichier par défaut trouvé")
                # print(regexp)
        except AttributeError:
            # print("Pas de fichier d'expressions régulières à appliquer par défaut au HTML")
            # print("On tente de voir si un fichier a été défini en argument")
            if regexp == "":
                # print("Pas d'argument non plus, on sort")
                return

        newzip = EpubStudio.clone(self)
        if os.path.exists(newzip):
            print("Deleting remaining temp file")
            os.remove(newzip)
        with zipfile.ZipFile(self.name, 'a') as content:
            for element in content.infolist():
                item = element.filename
                mime = MimeTypes()
                mime_type = mime.guess_type(item)[0]
                if mime_type == "application/xhtml+xml":
                    with content.open(item) as openfile:
                        data = openfile.read().decode('UTF-8')
                        dataclean = dude.cleantext(data ,dude.regexp(regexp))
                        with open(str(item), "w") as tempfile:
                            tempfile.write(dataclean)
                    with zipfile.ZipFile(newzip, 'a') as newcontent:
                        newcontent.write(item, item, zipfile.ZIP_DEFLATED)
                    garbage = os.path.split(element.filename)
                    os.remove(element.filename)
                    if garbage[0]:
                        os.removedirs(garbage[0])
                else:
                    content.extract(element.filename, os.getcwd())
                    # with open(str(item), "w") as tempfile:
                    #     tempfile.write(element)
                    if element.compress_type:
                        compressmode = element.compress_type
                    else:
                        compressmode = 0
                    with zipfile.ZipFile(newzip, 'a') as newcontent:
                        newcontent.write(element.filename, element.filename, compressmode)
                    garbage = os.path.split(element.filename)
                    os.remove(element.filename)
                    if garbage[0]:
                        os.removedirs(garbage[0])
        os.replace(newzip, self.name)
        # print("Application des expressions régulières faites")

    def list(self):
        with zipfile.ZipFile(self.name, 'r') as content:
            for item in content.infolist():
                print(item)
            print("-----------------------")
            for item in content.namelist():
                print(item)
                mime = MimeTypes()
                mime_type = mime.guess_type(item)[0]
                print("Mimetype : "+str(mime_type))
                try:
                    ext = "".join(item.split(".")[-1:])
                except:
                    ext = ""
                if ext in ["xhtml", "xml", "html", "opf", "ncx"]:
                    with content.open(item) as openfile:
                        tree = ET.parse(openfile)
                        print("TAG : " + str(tree.getroot().tag))
                        print("ATTRIB : " + str(tree.getroot().attrib))
                        print("Content:")
                        for element in tree.getroot():
                            print("\t"+ str(element))
                        print("")
                else:
                    print("")

    def cleantoc(self, regexp = ""):
        """
        Clean the content of xhtml files of an epub file with a regexp ini file
        """
        # On vérifie si on a déjà le fichier de regexp, ou s'il est en argument
        try:
            if self.regexp_epub:
                regexp = os.path.join(self.dir, "regexp_toc_ncx.ini")
                # print("Fichier par défaut trouvé")
                # print(regexp)
        except AttributeError:
            # print("Pas de fichier d'expressions régulières à appliquer par défaut au HTML")
            # print("On tente de voir si un fichier a été défini en argument")
            if regexp == "":
                # print("Pas d'argument non plus, on sort")
                return

        newzip = EpubStudio.clone(self)
        if os.path.exists(newzip):
            print("Deleting remaining temp file")
            os.remove(newzip)
        with zipfile.ZipFile(self.name, 'a') as content:
            for element in content.infolist():
                item = element.filename
                ext = "".join(item.split(".")[-1:])
                if ext == "ncx":
                    with content.open(item) as openfile:
                        data = openfile.read().decode('UTF-8')
                        dataclean = dude.cleantext(data ,dude.regexp(regexp))
                        with open(str(item), "w") as tempfile:
                            tempfile.write(dataclean)
                    with zipfile.ZipFile(newzip, 'a') as newcontent:
                        if element.compress_type:
                            compressmode = element.compress_type
                        else:
                            compressmode = 0
                        newcontent.write(element.filename, element.filename, compressmode)
                    os.remove(item)
                else:
                    content.extract(element.filename, os.getcwd())
                    if element.compress_type:
                        compressmode = element.compress_type
                    else:
                        compressmode = 0
                    with zipfile.ZipFile(newzip, 'a') as newcontent:
                        newcontent.write(element.filename, element.filename, compressmode)
                    garbage = os.path.split(element.filename)
                    os.remove(element.filename)
                    if garbage[0]:
                        os.removedirs(garbage[0])
        os.replace(newzip, self.name)

    def cleanopf(self, regexp = ""):
        """
        Clean the content of opf file of an epub file with a regexp ini file
        """
        # On vérifie si on a déjà le fichier de regexp, ou s'il est en argument
        try:
            if self.regexp_epub:
                regexp = os.path.join(self.dir, "regexp_opf.ini")
                # print("Fichier par défaut trouvé")
                # print(regexp)
        except AttributeError:
            # print("Pas de fichier d'expressions régulières à appliquer par défaut au HTML")
            # print("On tente de voir si un fichier a été défini en argument")
            if regexp == "":
                # print("Pas d'argument non plus, on sort")
                return

        newzip = EpubStudio.clone(self)
        if os.path.exists(newzip):
            print("Deleting remaining temp file")
            os.remove(newzip)
        with zipfile.ZipFile(self.name, 'a') as content:
            for element in content.infolist():
                item = element.filename
                ext = "".join(item.split(".")[-1:])
                if ext == "opf":
                    with content.open(item) as openfile:
                        data = openfile.read().decode('UTF-8')
                        dataclean = dude.cleantext(data ,dude.regexp(regexp))
                        modtime = time.strftime("%Y-%m-%dT%H:%M:%SZ",time.gmtime())
                        modified = "<meta property=\"dcterms:modified\">" + str(modtime) + "</meta>\n</metadata>"
                        dataclean = dude.cleantext(dataclean, [("</metadata>", modified)])
                        with open(str(item), "w") as tempfile:
                            tempfile.write(dataclean)
                    with zipfile.ZipFile(newzip, 'a') as newcontent:
                        if element.compress_type:
                            compressmode = element.compress_type
                        else:
                            compressmode = 0
                        newcontent.write(element.filename, element.filename, compressmode)
                    os.remove(item)
                else:
                    content.extract(element.filename, os.getcwd())
                    if element.compress_type:
                        compressmode = element.compress_type
                    else:
                        compressmode = 0
                    with zipfile.ZipFile(newzip, 'a') as newcontent:
                        newcontent.write(element.filename, element.filename, compressmode)
                    garbage = os.path.split(element.filename)
                    os.remove(element.filename)
                    if garbage[0]:
                        os.removedirs(garbage[0])
        os.replace(newzip, self.name)


########################################
#      Zone de test ci-dessous
########################################

if __name__ =='__main__':
    # reg = "/home/yann/Documents/Programmation/python/lib/epubstudio/test/regexp_epub.ini"
    # regopf ="/home/yann/Documents/Programmation/python/lib/epubstudio/test/regexp_opf.ini"
    # regtoc = "/home/yann/Documents/Programmation/python/lib/epubstudio/test/regexp_toc_ncx.ini"
    test = "/home/yann/Documents/Programmation/python/lib/epubstudio/test/ernaut_02.epub"

    ernaut1 = EpubStudio(test)

    ernaut1.cleanup()
    ernaut1.cleanopf()
    ernaut1.cleantoc()
