#!/usr/bin/python3
# -*- coding: utf-8 -*-

import re
import configparser

def readini(src):
    """
    Lit le fichier source .ini indiqué et retourne une liste de tuples
    du contenu :
    [(valeur_source1, valeur_finale_1), (valeur_source2, valeur_finale_2)...
    """
    rslt=[]
    config = configparser.ConfigParser()
    config.read(src)
    for section in config.sections():
        rslt.append((config.items(section)[0][1],config.items(section)[1][1]))
    return rslt

def cleantext(src,regexp):
    """
    Nettoie le texte reçu en src avec la liste de tuples d'expressions
    régulières indiquée en second.

    Retourne une chaîne de caractère.
    """
    for regle in regexp:
        src = (re.sub(re.compile(regle[0]), regle[1], src))
    return src

def applyregexpfile(src,regexp):
    """
    Nettoie une chaîne de caractère avec une liste d'expressions régulières
    extraites d'un fichiers .ini

    Retourne une chaîne de caractères.
    """
    return cleantext(src, readini(regexp))

# Zone de test
if __name__ =='__main__':
    original = "Mon texte original"
    src = "original"
    dst = "final"
    rules = [(src, dst)]
    print(original)
    print(cleantext(original, rules))
    print("OK")
