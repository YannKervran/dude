#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Epub Studio est un package de manipulation des epubs.
"""

from .epubstudio import *
from  .regexp import *

__all__ = ["epubstudio", "regexp"]
__version__ = "0.4"
