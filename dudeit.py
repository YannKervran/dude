#!/usr/bin/python3
# -*- coding: utf-8 -*-

import re
import os
import argparse
import yaml
import shutil
import stat
from unidecode import unidecode

#lib perso
from dudeit import dude
from epubstudio import epubstudio


###############################################################################
# Présentation de la fonction
###############################################################################

parser = argparse.ArgumentParser(
    description="script d'export depuis markdown vers epub et dokuwiki",
    epilog="""
     Il faut indiquer le fichier d'entrée avec son extension en .md

     On peut indiquer le nom du fichier de sortie
        avec l'argument \"o=\" (sans extension). Par défaut, ce sera
        le même nom que celui d'entrée.

     Il faut avoir un .ini dans le même répertoire que le fichier d'entrée,
        avec soit le nom du répertoire (générique),
        soit celui du fichier (spécifique, préféré au générique).
        On peut aussi en indiquer un précis, où qu'il soit,
        avec l'argument \"-i=\"
    """)

###############################################################################
# Liste des arguments possibles
###############################################################################
parser.add_argument("filesource", help="fichier source markdown",
                    type=str)
parser.add_argument("-o", "--output", help="nom des epubs générés, par défaut identique au fichier source",
                    type=str)
parser.add_argument("-ew", dest='full', action='store_true', help="génère epubs et pages Dokuwiki (défaut)")
parser.set_defaults(full=True)
parser.add_argument("-w", dest='wiki', action='store_true', help="ne génère que les pages Dokuwiki")
parser.add_argument("-e", dest='epub', action='store_true', help="ne génère que de l'epub")
parser.add_argument("-d", dest='dyslexic', action='store_true', help="génère aussi un epub avec police pour dyslexique (défaut si listé dans le .ini)")
parser.add_argument("-nd", dest='dyslexic', action='store_false', help="ne génère pas d'epub avec police pour dyslexique")
parser.set_defaults(dyslexic=True)
parser.add_argument("-i", "--ini", help="nom du fichier de configuration .ini à utiliser",
                    type=str)
args = parser.parse_args()

# On calcule quelques champs et on en déduit les paramètres

# Fichier de sortie
if not args.output:
    args.output =  re.sub(re.compile("(.*).md"),"\\1",(os.path.basename(args.filesource)))

# Fichier de configuration
if not args.ini:
        args.ini =  dude.pickini(args.filesource)
        inifile = args.ini # Nécessaire pour la méthode dude.compiledoku qui utilise des args aussi

# Test si il faut sortir les epubs et les wikis
if args.full:
    if args.epub:
        args.wiki = False
    elif args.wiki:
        args.epub = False
    else:
        args.epub = True
        args.wiki = True

args.output = unidecode(args.output)

###############################################################################
# Préparation des données
###############################################################################

# compilation du fichier markdown à exporter, on le nettoie avec les regexp
rawprocess = dude.makebase(dude.sourceliste(args.filesource, args.ini))
process = dude.cleanup(rawprocess, dude.regexp(dude.parseini(args.ini)['Regexpfile']['regexp']))

############## Préparation des métadonnées et insertion dans le fichier de base
# On définit la date
date = dude.defdate(process)

# On récupère les métadonnées yaml et on les parse
metaepubfull="".join(dude.meta_text(process)[0])
metaepub = yaml.load(metaepubfull)

# On récupère le texte seul, sans méta
mytext = dude.meta_text(process)[1]

# On définit le titre et la date, selon les cas
mytitle = "Qit'a"# Titre par défaut

# Si pas d'abstract, utilisation du champ Description par defaut, sinon
# c'est abstract
if metaepub['abstract']:
    metaepub['description'] = metaepub['abstract']
try:
    # On vérifie si on est dans une compilation de Qit'a
    # et dans ce cas on actualise les infos de titre
    if metaepub['abstract'] == "Compilation de textes.":
        mytitle = "Recueil Qit'a"
        metaepub["title"] = mytitle +" - " + date + "\n"
        metaepub["parution"] = date + "\n"
        metaepub["shorttitle"] = mytitle + "\n"
    else:
        # Si ce n'est pas une simple compilation
        # On ne touche à rien
        for line, n in zip(mytext, range(len(mytext))):
            if re.match(re.compile("[=]+"), line):
                mytitle = mytext[(n-1)][:-1]
        metaepub["title"] = mytitle +" - " + date + "\n"
        metaepub["parution"] = date + "\n"
        metaepub["shorttitle"] = mytitle + "\n"
        pass
except KeyError:
    # Si il n'y a pas de metadonnées, il faut les créer
    # On considère que c'est un Qit'a simple
    for line, n in zip(mytext, range(len(mytext))):
        if re.match(re.compile("[=]+"), line):
            mytitle = mytext[(n-1)][:-1]
    # on ajoute un "title" et la date de parution dans les metadata
    # (pour le titre de l'epub et la page de couverture du gabarit)
    metaepub['title'] = "Qit'a - "+ date + " - " + mytitle +"\n"
    metaepub["parution"] = date + "\n"
    metaepub["shorttitle"] = mytitle + "\n"


# On vérifie qu'on a bien deux sauts de ligne encadrant le bloc de meta
# en début de fichier
if (mytext[0:1]) != ["\n"]:
    mytext.insert(0, "\n")
if (mytext[0:2]) != ["\n", "---\n"]:
    mytext.insert(1, "---\n")
if (mytext[0:3]) != ["\n", "---\n", "---\n"]:
    mytext.insert(2, "---\n")
if (mytext[0:4]) != ["\n", "---\n", "---\n", "\n"]:
    mytext.insert(3, "\n")

# On remet les metas en début de texte
for key in metaepub:
    # print("{}: {}\n".format(key, metaepub[key])) # TEST TODO
    mytext.insert(2, "{}: {}\n".format(key, metaepub[key]))

###############################################################################"
# Export des epubs
###############################################################################

if args.epub:
    # On définit les fichiers de destination des différents fichiers générés
    epubdepot = dude.parseini(args.ini)['Destination']['epubdepot']
    epubdoku = dude.parseini(args.ini)['Destination']['epubdoku']
    for modeleexport in dude.epubs(args.ini)[1]:
        if modeleexport == "dys" and not args.dyslexic:
            pass
        else:
            argum = (dude.epubs(args.ini)[1][modeleexport][1])
            version = dude.epubs(args.ini)[0]
            outepub = (os.path.join(epubdepot,args.output.lower()+dude.epubs(args.ini)[1][modeleexport][0]+".epub"))
            dude.epubexport("".join(mytext), outepub, argum, version)
            # Passage à epubstudio pour netoyer avec des expressions régulières
            # Pour chaque type, cela ne se fait que si un fichier de regexp est indiqué dans le ini
            epubobject = epubstudio.EpubStudio(outepub)
            try:
                epubobject.cleanup(dude.epubs(args.ini)[1][modeleexport][2]["regexp_epub"])
            except KeyError:
                pass
            try:
                epubobject.cleanopf(dude.epubs(args.ini)[1][modeleexport][2]["regexp_opf"])
            except KeyError:
                pass
            try:
                epubobject.cleantoc(dude.epubs(args.ini)[1][modeleexport][2]["regexp_toc_ncx"])
            except KeyError:
                pass

            # On copie le ficher outepub obtenu dans le dossier epubdoku
            shutil.copy(outepub, os.path.join(epubdoku,args.output.lower()+dude.epubs(args.ini)[1][modeleexport][0]+".epub"))
            # On met les autorisations au groupe (il faut que le répertoire destinataire ait un ACL qui donne le groupe www-data aux fichiers copiés là-bas)
            os.chmod(os.path.join(epubdoku,args.output.lower()+dude.epubs(args.ini)[1][modeleexport][0]+".epub"), stat.S_IRUSR | stat.S_IWUSR| stat.S_IXUSR|stat.S_IRGRP | stat.S_IWGRP| stat.S_IXGRP| stat.S_IROTH)


###############################################################################
# Export des pages dokuwiki
###############################################################################

if args.wiki:
    # On définit les fichiers de destination des différents fichiers générés
    textdokurep = dude.parseini(args.ini)['Destination']['textdokurep']
    textdokusum = dude.parseini(args.ini)['Destination']['textdokusum']
    textprvdokurep = dude.parseini(args.ini)['Destination']['textprvdokurep']


    # création du fichier markdown à exporter
    rawprocesswiki = dude.makebase(dude.sourceliste(args.filesource, args.ini, doku = True))
    processwiki = dude.cleanup(rawprocesswiki, dude.regexp(dude.parseini(args.ini)['Regexpfile']['regexp']))


    ###################  Génération des pages annonce et sommaire
    # Récupération des metadata nécessaires
    metafull= "".join(dude.meta_text(processwiki)[0])
    meta = yaml.load(metafull)
    try:
        summary = re.sub(re.compile("([0-9-]{4,9})\."),"\\1\\.",re.sub(re.compile("\n")," ",meta["abstract"]))
    except KeyError:
        summary = "Pas de résumé"

    try:
        characters = meta["characters"].split(', ')
    except KeyError:
        characters = []

    try:
        rights = re.sub(re.compile("\n")," ",meta["rights"])
    except KeyError:
        rights = "BY - SA"

    lienspersos = dude.pagewikilist("pagelink.ini", characters, "fr:falconus:personnages:")

    pagesdates= dude.datestexte(args.filesource)
    liensdates = ""
    for date in pagesdates:
        liensdates += "  * [[{0}]]\n".format(date)

    ########## Génération des contenus des pages Dokuwiki
    # dokutexte = chaîne de caractères de la page du Qit'a à lire en ligne
    # dokupr = chaîne de caractères de la page rpésentant le contenu du Qit'a
    # dokusum = chaîne de caractères de la page qui est ensuite incluse dans
    #           la présentation des Qit'a

    # Préparation du texte de la page Dokuwiki contenant le texte
    dokutexte = dude.dokuexport("".join(processwiki))
    dokutexteTemplate = dude.ajoutencart(dokutexte)

    # Insertion des mentions supplémentaires
    prevFile = dude.recentfile(args.ini)
    encart1 = ""
    if prevFile == "":
        precedent = ""
    else :
        precedent = "  * <wrap qitabefore>[[fr:qita:"+prevFile+"]]</wrap>\n"

    sortie = dude.compiledoku(dude.parseini(inifile)['Dokutemplate']['templatefolder'] + "/" + "dokuTemplateEncart1.txt", args.output, precedent)
    dokutexte = dude.compiledoku(dokutexteTemplate, sortie)

    # Préparation du texte de la page de gestion interne, privée
    dokupr = dude.compiledoku(dude.parseini(inifile)['Dokutemplate']['templatefolder'] + "/" + "dokuTemplate1pr.txt", mytitle, rights, summary, args.output, lienspersos, liensdates)

    # Préparation du texte de la page de présentation
    # qui est ensuite mise dans un include

    dokusum = dude.compiledoku(dude.parseini(inifile)['Dokutemplate']['templatefolder'] + "/" + "dokuTemplate2sum.txt", mytitle, meta["abstract"], args.output, "{{:fr:qita:"+args.output+".epub"+"|"+ mytitle +" - epub standard}}",  "{{:fr:qita:"+args.output+"_dys.epub"+"|"+ mytitle +" - epub pour dyslexiques}}", mytitle, dude.defdate(process))

    # Écriture des pages Dokuwiki

    # Page du texte principal
    with open(textdokurep+"/"+args.output+".txt", "w") as infile:
        infile.write(dokutexte)
    # Ajout de la mention vers ce nouveau fichier dans le fichier texte Qit'a précédent
    if prevFile :
        with open(textdokurep+"/"+prevFile+".txt", "r+") as infile:
            content = infile.read()
            modif = re.sub(r"(\* <wrap qitabefore>.*</wrap>)", "\\1\n  * <wrap qitaafter>[[fr:qita:"+args.output+"]]</wrap>", content)
        with open(textdokurep+"/"+prevFile+".txt", "w") as infile:
            infile.write(modif)

    # Page de présentation interne
    with open(textprvdokurep+"/"+(args.output+"_pr.txt"), "w") as infile:
        infile.write(dokupr)

    # Page de résumé, pour les includes de présentation
    with open(textdokusum+"/"+(args.output+"_sum.txt"), "w") as infile:
        infile.write(dokusum)
