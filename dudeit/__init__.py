#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
dudeit est un package de génération d'epub et de pages dokuwiki.
"""

from .dude import *

__all__ = ["dude"]
__version__ = "0.9"
