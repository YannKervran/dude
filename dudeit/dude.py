#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pypandoc
import datetime
import locale
import configparser
import re
import os
import calendar

def parseini(src):
	"""Lit le fichier source .ini indiqué et retourne
	un dictionnaire.
	Chaque section est rendue sous forme de dictionnaire
	sauf la section 'Sourcefiles' qui peut avoir besoin
	d'être ordonnée et qui est donc en liste
	"""
	rslt={}
	config = configparser.ConfigParser()
	config.read(src)
	for section in config.sections():
		if section == 'Sourcefiles':
			itemlist = []
			for item in config.items(section):
				itemlist.append(item)
			rslt[section] = itemlist
		else:
			itemlist = {}
			for item in config[section]:
				itemlist[item] = config[section][item]
			rslt[section] = itemlist
	return rslt

def epubs(fichierini):
	"""
	Cette fonction accepte un fichier de configuration
	.ini en entrée.

	Elle retourne une liste :
	- le premier élément est la version d'export ("epub" ou "epub3") ;
	- le second élément est un dictionnaire avec
	le nom du modèle en clef et comme valeurs la liste
	des arguments à passer à l'exportation epub
	et un éventuel suffixe au nom de l'epub
	Pour le modèle de base, le suffixe renvoyé est
	vide
	- le troisième élément est un dictionnaire des fichiers à utiliser avec epubstudio
	pour appliquer des expressions régulières au fichier epub obtenu, les clefs sont
	les suivantes :
		- regexp_epub for changes in XHTML files ;
		- regexp_opf for changes in opf file ;
		- regexp_toc_ncx for changes in ncx files.
	"""
	listeexport={}
	version = "epub"
	data = parseini(fichierini)
	for section in data:
		if section[0:8] == "Epubargs":
			listeargs = []
			epubstudiodic = {}
			modele = section[8:]
			args = ('Epubargs'+modele)
			for element in data[args]:
				if element == "epubversion":
					getversion = data[args]["epubversion"]
					if getversion == "3":
						version = "epub3"
				elif element == "standalone" or element == "smart" or element =="toc":
					if data[args][element]:
						listeargs.append("--"+element)
				elif element[0:7] == "regexp_":
					epubstudiodic[element] = data[args][element]
				else:
					listeargs.append("--"+element+"="+data[args][element])
			fonts = ('Embedfonts'+modele)
			for element in data[fonts]:
				listeargs.append("--epub-embed-font="+data[fonts][element])
			if modele !='base' :
				suffixe =  "_"+modele
				listeexport[modele]= (suffixe, listeargs, epubstudiodic)
			else:
				listeexport[modele]= ("", listeargs, epubstudiodic)
	return[version, listeexport]

def makebase(listefichier):
	"""Cette fonction accepte une liste de fichiers .md et/ou .yaml
	en entrée et compile le tout avec pypandoc pour en faire un
	fichier markdown prêt à être modifié/exporté pour différents formats.
	Elle retourne une liste qui contient toutes les lignes du document.
	"""
	locale.setlocale(locale.LC_ALL, '')
	sourcelist = []
	metalist = []
	inmeta = False
	# création des listes à exporter, une pour les meta, l'autre pour les textes
	for file in listefichier:
		with open(file, "r") as infile:
			for line in infile:
				if line == "---\n" and inmeta == False:
					inmeta = True
					pass
				elif line == "---\n" and inmeta ==True:
					inmeta = False
					pass
				if inmeta == True:
					if line != "---\n":
						metalist.append(line)
				elif inmeta == False:
					if line != "---\n":
						sourcelist.append(line)
		sourcelist.append("\n")
	# Il faut ensuite vérifier les métas
	# Si il existe plusieurs fois le même type, c'est une compilation
	# Dans le cas d'une compilation, des metada types seront définies
	# Si c'est un seul document, ses metadata seront conservées
	metatypes =[]
	multimeta = False
	for entree in metalist:
		meta = ""
		contenu = ""
		for letter in entree:
			if letter != ":":
				meta = meta + letter
			else:
				break
		if meta not in metatypes:
			metatypes.append(meta)
		else:
			multimeta = True
			break
	if multimeta == True:
		datemeta = "date: "+datetime.datetime.now().date().strftime("%Y-%m-%d").capitalize()+"\n"
		metalist = [datemeta, 'abstract: Compilation de textes.\n']

	metaline = ["---\n"]
	metalist.extend(metaline)
	metaline.extend(metalist)

	# Recréation d'un document avec en-tête yaml

	sourcelist = metaline + sourcelist
	# compilation d'une chaîne markdown avec toutes les données et des lignes non tronquées
	tempmd = pypandoc.convert("".join(sourcelist), format="md", to="markdown", extra_args=["--smart", "--standalone", "--columns=99999999"])
	tempmdlist = tempmd.split('\n')
	result = []
	for line in tempmdlist:
		result.append(line+'\n')
	return result

def regexp(src):
	"""
	Lit le fichier source .ini indiqué et retourne une liste de tuples
	du contenu :
	[(valeur_source1, valeur_finale_1), (valeur_source2, valeur_finale_2)] etc.
	"""
	rslt=[]
	config = configparser.ConfigParser()
	config.read(src)
	for section in config.sections():
		rslt.append((config.items(section)[0][1], config.items(section)[1][1]))
	return rslt

def cleanup(src,regexp):
	"""Nettoie la liste indiquée en entrée
	en tenant compte de la liste de regexp à appliquer
	et retourne une liste
	"""
	dest = []
	for line in src:
		destline = line
		for regle in regexp:
			destline = (re.sub(re.compile(regle[0]),regle[1],destline))
		dest.append(destline)
	if dest[0] != "\n":
		dest.insert(0,"\n")
	if dest[len(dest)-1] != "\n":
		dest.append("\n")
	result = []
	for line in dest:
		result.append(line)
	return result

def cleantext(src,regexp):
	"""clean-up the src string and return a string modified by regexp list
	"""
	for regle in regexp:
		src = (re.sub(re.compile(regle[0]),regle[1],src))
	return src

def recentfile(ini):
	"""
	Fonction qui détermine le fichier le plus récent (hormis "start.txt")
	dans le répertoire de destination de dokuwiki ["Destination"]["textdokurep"]
	indiqué dans l'ini.
	Soit c'est sa date de modification, soit celle de création, la plus récente,
	qui est prise pour faire le tri
	Il retourne le nom du fichier sans l'extension
	"""
	textemod = parseini(ini)
	bigger = 0
	result = ""
	for entry in os.scandir(textemod["Destination"]["textdokurep"]):
		if not entry.name.startswith('.') and entry.is_file() and entry.name != "start.txt" and entry.name[-4:] == ".txt":
			creation = int(re.sub(r"^[a-zA-Z0-9]*_([0-9]*)_.*$", "\\1", entry.name))
			if creation > bigger:
				bigger = creation
				result = re.sub(re.compile(r"\.[a-z0-9]{3,4}$"), "", entry.name)
	return result

def pickini(fichier):
	"""
	Vérifie l'existence d'un fichier .ini ayant
	le même basename que le fichier indiqué,
	ou, à défaut, la présence d'un fichier .ini
	dans le répertoire, nommé comme le répertoire.
	Quitte le programme en cas d'échec.
	"""
	ini = re.sub(re.compile("(.*).md"), (os.path.join(os.path.normcase(os.path.dirname(fichier)),"\\1.ini")),(os.path.basename(fichier)))
	baseini = os.path.basename(os.getcwd())+".ini"
	if os.path.isfile(os.path.abspath(ini)):
		return ini
	elif os.path.isfile(os.path.abspath(baseini)):
		return baseini
	else:
		print("Pas de fichier de configuration .ini")
		exit()

def datestexte(fichier):
	"""Parcourt un fichier .md pour y trouver des dates indiquées
	dans les niveaux de titres, sous le format :
	lundi 01 février 1148, par exemple
	Puis en fais une série de liens dokuwiki
	qui renvoient vers les pages crées par
	le plugin YearBox, sous forme d'une liste.
	"""
	# On définit la localisation
	locale.setlocale(locale.LC_ALL, '')

	# On prépare une expression régulière en fonction de la localisation
	# Le but est de détecter jour du mois - mois - année (sur 4 chiffres)

	# Liste des mois
	annee = calendar.month_name[1]
	for i in range(2,13):
		annee += "|"+calendar.month_name[i]

	# Création de l'expression régulière
	regexp = "^[#]+.*("+annee+") [0-9]{4}$"
	pattern = re.compile(regexp)

	#On cherche toutes les lignes qui correspondent dans le fichier .md source
	listedates = []
	with open(fichier, "r") as source:
		for line in source:
			if pattern.search(line):
				# On vire certains caratères qui embrouillent
				line = re.sub("\xa0", " ", line)
				listedates.append(line)

	jours = []
	for result in listedates:
		selection = r"(.*) ([0-9]{1,2})[er]{0,2} ("+annee+") ([0-9]{4})(.*)\n"
		jours.append(re.sub(selection, r' \2 \3 \4 \5', result))

	goodlist= []
	for jour in jours:
		goodlist.append((jour, datetime.datetime.strptime(jour, ' %d %B %Y ')))

	newline = []
	for line in goodlist:
		newline.append((line[1].strftime('%Y-%m'),line[1].strftime('%Y-%m-%d')))

	results = []
	for result in newline:
		results.append("fr:falconus:chronologie:{0}:jour-{1}".format(result[0],result[1]))

	return results

def defdate(liste):
	""" Trouve la date au format yaml dans la liste
	et retourne une chaîne au format local de type:
	Mois - Année complète :"Février 2016"
	"""
	locale.setlocale(locale.LC_ALL, '')
	# On recherche la date indiquée dans la zone yaml
	for line in liste:
		date_attendue = re.search(re.compile("[0-9]{4}-[0-9]{2}-[0-9]{2}"), line)
		if date_attendue:
			datebase = re.compile("[0-9]{4}-[0-9]{2}-[0-9]{2}").search(date_attendue.group()).group()
			dstdate = datetime.date(int(datebase[0:4]), int(datebase[5:7]), int(datebase[8:])).strftime("%B %Y").capitalize()
			break
		else:
			#Utilise la date du jour
			dstdate = datetime.datetime.now().date().strftime("%B %Y").capitalize()
	return dstdate

def meta_text(liste):
	"""
	Récupère les métas de type yaml dans une liste
	Encadrées par une ligne "---" ou "..." et séparées
	du corps de texte par une ligne vide.
	Il retourne un tuple constitué des meta et du texte, séparés
	"""
	meta = []
	text = []
	# On récupère le contenu des metas d'abord
	for line, index in zip(liste, range(0, len(liste))):
		if line == "---\n" or line == "...\n":
			indexmeta = index + 1
			while liste[indexmeta] != "---\n" and liste[indexmeta] != "...\n":
				meta.append(liste[indexmeta])
				indexmeta += 1
			break
	# En fonction de la longueur des metas, on récupère le texte
	# Amputé de cette longueur + 2 entrées (les lignes "---\n")
	for line, index in zip(liste, range(0, len(liste))):
		if line == "---\n" or line == "...\n":
			indexmeta = index + len(meta) + 2
			while indexmeta < len(liste):
				text.append(liste[indexmeta])
				indexmeta += 1
			break

	return (meta, text)

def sourceliste(entree, ini = "", doku = False):
	"""
	Accepte un fichier markdown en entrée,
	éventuellement un .ini, sinon par défaut, ce sera
	le même nom que le markdown
	L'option doku permet de ne pas tenir compte
	des indications 'licence' de
	la section 'Sourcefiles' du .ini
	La fonction retourne une liste des fichiers
	à utiliser pour l'export
	"""
	if ini == "":
		ini =  re.sub(re.compile("(.*).md"), (os.path.join(os.path.normcase(os.path.dirname(entree)),"\\1.ini")),(os.path.basename(entree)))
	lstsource = [os.path.join(os.path.normcase(os.path.dirname(entree)),(os.path.basename(entree)))]
	dico = parseini(os.path.join(os.path.normcase(os.path.dirname(ini)),(os.path.basename(ini))))
	for liste in dico['Sourcefiles']:
		if doku == True and ( liste[0] == 'licence'):
			pass
		else:
			lstsource.append(liste[1])
	return lstsource

def pagewikilist(inifile, characters, prefix = ""):
	""" Cette fonction accepte un fichier ini en entrée
	qui comporte l'équivalence entre des noms et des pages wiki
	une liste de noms
	un éventuel préfixe à inclure en préfixe de chaque item de liste
	et retourne une chaîne contenant des liens ordonnés
	en liste dokuwiki
	"""
	persos={}
	parsefile = configparser.ConfigParser()
	parsefile.read(inifile)
	for section in parsefile.sections():
		for item in parsefile.items(section):
			persos[item[0]] = item[1]
	lienspersos = ""
	for character in characters:
		ajoutlien = ""
		for key in persos.keys():
			if key == character.lower():
				ajoutlien = "  * [[{0}{1}|{2}]]\n".format(prefix, persos[character.lower()], character)
		if ajoutlien == "":
			ajoutlien = "  * [[{0}{1}]]\n".format(prefix, character.lower())
		lienspersos += ajoutlien
	return lienspersos

def epubexport(entree,sortie,arguments, version):
	"""
	Fonction qui accepte une chaîne markdown en entrée
	et qui crée un fichier epub correspondant à la sortie.
	- Il faut lui passer les paramètres issus de la fonction "epub",
	qui retourne une liste avec la version d'epub en entrée
	et les paramètres dans un dictionnaire en second
	"""

	pypandoc.convert(entree, format="md", to=version, outputfile=(sortie), extra_args=arguments)

def dokuexport(entree):
	"""
	Fonction qui accepte une chaîne markdown en entrée
	et qui crée une chaîne de caractère dokuwiki correspondant en sortie.
	"""

	sortie = pypandoc.convert(entree, format="md", to="dokuwiki", extra_args=["--smart"])
	return sortie

def compiledoku(template = "", *args):
	"""
	Prépare un texte DokuWiki en fonction d'un template
	qui contient les placements des arguments sous la forme {0},{1} etc.
	et la liste des arguments à y placer, dont le numéro d'index sera
	déterminé en fonction de leur ordre.
	Si jamais il manque un argument, la fonction en ajoute un vide
	ou s'il y en a trop, la fonction n'utilise pas les éléments en trop.
	Si le fichier indiqué en entrée ''template'' n'existe pas,
	la méthode pensera qu'il s'agit d'une simple chaîne à laquelle appliquer
	les changements.
	Attention, le recours à la méthode ''format'' oblige à échapper
	les accolades qu'on veut garder : il faut en mettre 2 pour en avoir une
	dans le résultat.
	"""
	if template == "":
		print("Gabarit non indiqué.")
	elif os.path.isfile(template):
		result = ""
		with open(template, "r") as infile:
			# On liste les entrées à changer
			listRemplace = re.findall(r".*{[0-9]*}.*", infile.read())
			# On compte le nombre d'entrées
			countRempList = []
			for elem in listRemplace:
				countRempList.append(int(re.sub(r".*{([0-9]*)}.*", "\\1", elem)))
			countRemp = 0
			for i in countRempList:
				if i > countRemp :
					countRemp = i
			# On compare au nombre d'arguments
			# S'il y en a trop peu, on en ajoute des vides à la fin
			listArg = []
			for arg in args:
				listArg.append(arg)
			if len(listArg) < countRemp:
				for i in range(0, (countRemp - len(listArg) + 1)):
					listArg.append("")
			# On formate le contenu du document de sortie
			with open(template, "r") as templateFile:
				arguments = tuple(listArg)
				result = templateFile.read().format(*arguments)
				return(result)
	else :
		result = ""
		# On liste les entrées à changer
		listRemplace = re.findall(r".*{[0-9]*}.*", template)
		# On compte le nombre d'entrées
		countRempList = []
		for elem in listRemplace:
			countRempList.append(int(re.sub(r".*{([0-9]*)}.*", "\\1", elem)))
		countRemp = 0
		for i in countRempList:
			if i > countRemp :
				countRemp = i
		# On compare au nombre d'arguments
		# S'il y en a trop peu, on en ajoute des vides à la fin
		listArg = []
		for arg in args:
			listArg.append(arg)
		if len(listArg) < countRemp:
			for i in range(0, (countRemp - len(listArg) + 1)):
				listArg.append("")
		# On formate le contenu du document de sortie
		arguments = tuple(listArg)
		result = template.format(*arguments)
		return(result)

def ajoutencart(content):
	"""
	Ajoute un encart au début de la page présentant le texte Qita
	Insère le marqueurs {0} pour ensuite faire un compileDoku.
	"""
	rslt = re.sub(" ======\n", "======\n{0}\n",content)
	return rslt
