# Dude

Ensemble de scripts servant à générer différents formats depuis le markdown, en suivant des procédures complexes de configuration.

Les formats de sortie possibles sont :

+ epub
+ page dokuwiki

Il est possible de générer plusieurs fichiers de chaque type, selon des paramètres personnalisables.

Toute la configuration se fait avec des fichiers .ini

Version 1.0 - 11/10/2017

## Utilisation

Il y a un virtualenv mis en place pour utiliser la lib. Pour l'activer, aller dans le répertoire dude puis taper :

    /bin/activate

Pour en sortir, il suffit de taper :

    deactivate

On peut tester le fonctionnement en allant dans le répertoire testing_zone puis en lançant la commande :

    python3 ../dudeit.py qita_043_marcheurs_de_lumiere.md

Les fichiers générés sont récupérés dans testing_zone/results

Les fichiers placés dans testing_zone sont en CC BY SA, issus de mes textes autour de la Méditerranée des croisades : https://ernautdejerusalem.fr  
Vous pouvez récupérer tous les formats sources sur le dépôt [Hexagora](https://framagit.org/YannKervran/Hexagora).

## Pourquoi ce nom ?

Comme il s'agit de mon premier projet en python, j'avais envie que les choses se passent bien, j'ai donc appliqué un précepte dudeiste : _Take it easy, man_  
Visitez [http://dudeism.com/](http://dudeism.com/) pour en savoir plus.


## Changelog
### Version 1.0
Version fonctionnelle utilisée pour la génération des epub et pages Dokuwiki du projet [Hexagora](https://hexagora.fr).
